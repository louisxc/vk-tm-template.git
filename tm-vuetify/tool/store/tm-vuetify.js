import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// 定义不需要永久存储的目录，即下次APP启动数据会自动清空，值为在modules目录下的文件名
let notSaveStateKeys = ['$error'];

let moduleTrue = {};

let lifeData = uni.getStorageSync('lifeData') || {};
// 为了兼容如果用户，不按规范创建，或者不使用vuex时就可略过导入用户的模块。
try {
	const modulesList = require.context('@/store/modules', true, /\.js$/);

	if (typeof modulesList === 'function' && typeof modulesList !== 'undefined') {
		// 加载modules目录下所有文件(分模块)
		const modules = modulesList.keys().reduce((modules, modulePath) => {
			const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
			const value = modulesList(modulePath)
			modules[moduleName] = {
				namespaced: true,
				...value.default
			}

			return modules

		}, {});
		moduleTrue = modules;
	}

	for (let moduleName in moduleTrue) {
		if (notSaveStateKeys.indexOf(moduleName) === -1) {
			if (!lifeData[moduleName]) lifeData[moduleName] = {};
		}
	}

	uni.setStorageSync('lifeData', lifeData);

	// 保存变量到本地存储中


} catch (e) {
	//TODO handle the exception
	// console.warn('tmui提醒：用户未使用vuex')
	console.error('如果未使用vuex，不用理会，如果使用了vuex报错请检错误信息：', e);
}
let pdefault_cookies_color = uni.getStorageSync('setTmVuetifyColor')
let pdefault_cookies_black = uni.getStorageSync('setTmVuetifyBlack')

const saveLifeData = function(key, value) {
	// 判断变量名是否在需要存储的数组中
	if (notSaveStateKeys.indexOf(key) === -1) {
		// 获取本地存储的lifeData对象，将变量添加到对象中
		let tmp = uni.getStorageSync('lifeData');
		// 第一次打开APP，不存在lifeData变量，故放一个{}空对象
		tmp = tmp ? tmp : {};
		tmp[key] = value;
		// 执行这一步后，所有需要存储的变量，都挂载在本地的lifeData对象中
		uni.setStorageSync('lifeData', tmp);
	}
}



const store = new Vuex.Store({
	modules: {
		...moduleTrue
	},
	// 如果是开发环境,则开启严格模式
	strict: process.env.NODE_ENV === 'development',
	state: {
		tmVuetify: {
			color: typeof pdefault_cookies_color === 'string' ? pdefault_cookies_color : '',
			black: typeof pdefault_cookies_black === 'boolean' ? pdefault_cookies_black : false,
			tmVueTifly_pages: '',
			tmVueTifly_pagesIndex: '',
			//这里是微信小程序和微信H5的配置资料。
			wxshareConfig_miniMp: {
				title: '', // 分享标题
				desc: '', // 描述
				imageUrl: '', // 分享图片
				path: '', // 分享路径
				copyLink: '', // 复制链接
				query: {}, // 分享参数
			}
		},
	},
	getters: {
		// $tm:state=>{
		// 	return $tm;
		// }
	},
	mutations: {
		updateStore(state, payload) {
			// 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
			if (typeof payload.value === "undefined") payload.value = "";
			let nameArr = payload.name.split('.');
			let saveKey = '';
			let len = nameArr.length;
			if (len >= 2) {
				let obj = state[nameArr[0]];
				for (let i = 1; i < len - 1; i++) {
					let keyName = nameArr[i];
					if (typeof obj[keyName] !== "object") obj[keyName] = {};
					obj = obj[keyName];
				}
				obj[nameArr[len - 1]] = JSON.parse(JSON.stringify(payload.value));
				saveKey = nameArr[0];
			} else {
				// 单层级变量，在state就是一个普通变量的情况
				state[payload.name] = JSON.parse(JSON.stringify(payload.value));
				saveKey = payload.name;
			}
			// 保存变量到本地，见顶部函数定义
			saveLifeData(saveKey, state[saveKey])
		},
		setTmVuetifyColor(state, color) {
			Vue.set(state.tmVuetify, 'color', color)
		},
		setPageNow(state, url) {
			Vue.set(state.tmVuetify, 'tmVueTifly_pages', url);
		},
		setPageNowIndex(state, index) {
			Vue.set(state.tmVuetify, 'tmVueTifly_pagesIndex', index);
		},
		setTmVuetifyBlack(state, black) {
			Vue.set(state.tmVuetify, 'black', black)
			if (black === true) {
				uni.setTabBarStyle({
					backgroundColor: "#212121"
				})
			} else {
				uni.setTabBarStyle({
					backgroundColor: "#FFFFFF"
				})
			}
		},
		setWxShare(state, cfg) {

			let pcf = cfg || {};
			if (typeof pcf !== 'object' || Array.isArray(cfg)) pcf = {};
			Vue.set(state.tmVuetify, 'wxshareConfig_miniMp', {
				...state.tmVuetify.wxshareConfig_miniMp,
				...pcf
			});

		}
	}
})

export default store;

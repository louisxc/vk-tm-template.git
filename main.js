import App from './App'
import config from '@/app.config.js'



// 引入 vk框架前端
import vk from './uni_modules/vk-unicloud';
// 引入 vk框架前端
Vue.use(vk);
// 初始化 vk框架
Vue.prototype.vk.init({
	Vue, // Vue实例
	config, // 配置
});

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'

import tmVuetify from "./tm-vuetify";
Vue.use(tmVuetify)

import tabBar from './pages/index/tabBar.vue'
Vue.component('tabBar', tabBar)

const app = new Vue({
	...App
})
app.$mount()
// #endif


// #ifdef VUE3
import {
	createSSRApp
} from 'vue'

export function createApp() {
	const app = createSSRApp(App)

	// 引入 vk框架前端
	app.use(vk);

	// 初始化 vk框架
	app.config.globalProperties.vk.init({
		Vue: app, // Vue实例
		config, // 配置
	});
	return {
		app
	}
}
// #endif
